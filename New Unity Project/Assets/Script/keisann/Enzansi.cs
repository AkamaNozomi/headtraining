using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//ボタンを使用するためUIとSceneManagerを使用ためSceneManagementを追加
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Enzansi : MonoBehaviour
{
    // ボタンをクリックするとEnzanshiSceneに移動します
    public void PushStartButton()
    {
        SceneManager.LoadScene("EnzanshiScene");
    }
}