using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace CsvRead
{
    public class Jiji_70_PlayScene : MonoBehaviour
    {
        class jiji_70_PlayScene
        {
            static void Main()
            {
                StreamReader sr = new StreamReader(@"Quiz_70.csv");
                {
                    //末尾まで繰り返す
                    while (!sr.EndOfStream)
                    {
                        //csvファイルの一行を読み込む
                        string line = sr.ReadLine();
                        //読み込んだ一行をカンマ毎に配列に格納する
                        string[] values = line.Split(',');

                        //配列からリストに格納する
                        List<string> lists = new List<string>();
                        lists.AddRange(values);

                        // コンソールに出力する
                        foreach (string list in lists)
                        {
                            System.Console.Write("{0} ", list);
                        }
                        System.Console.WriteLine();
                    }
                    System.Console.ReadKey();
                }
            }
        }
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}