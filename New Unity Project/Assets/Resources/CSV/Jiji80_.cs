using UnityEngine;
using System.IO;

public class Jiji80_ : MonoBehaviour
{
    void Start()
    {

        TextAsset csv = Resources.Load("CSV/Quiz_80") as TextAsset;

        StringReader reader = new StringReader(csv.text);
        while (reader.Peek() > -1)
        {
            string line = reader.ReadLine();
            string[] values = line.Split(',');
            for (int i = 0; i < values.Length; i++)
            {
                Debug.Log(values[i]);
            }
        }
    }
}